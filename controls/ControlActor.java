/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controls;

import actors.LayeredActor;
import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.GreenfootImage;
import greenfoot.World;
import worlds.LayeredWorld;
import worlds.layers.LayerPosition;

/**
 *
 * @author Michael
 */
public class ControlActor extends LayeredActor {
    public ControlActor() {
        setImage(new GreenfootImage("\\images\\void\\VoidImage 20x100.png"));
    }
    public void act() {
        if(Greenfoot.isKeyDown("w")) {
            World world = getWorld();
            if(world instanceof LayeredWorld) {
                LayeredWorld lWorld = (LayeredWorld) world;
                lWorld.getLayer(LayerPosition.STAGE).moveActors(0, 1);
            }
        }
        if(Greenfoot.isKeyDown("s")) {
            World world = getWorld();
            if(world instanceof LayeredWorld) {
                LayeredWorld lWorld = (LayeredWorld) world;
                lWorld.getLayer(LayerPosition.STAGE).moveActors(0, -1);
            }
        }
        if(Greenfoot.isKeyDown("d")) {
            World world = getWorld();
            if(world instanceof LayeredWorld) {
                LayeredWorld lWorld = (LayeredWorld) world;
                lWorld.getLayer(LayerPosition.STAGE).moveActors(1, 0);
            }
        }
        if(Greenfoot.isKeyDown("a")) {
            World world = getWorld();
            if(world instanceof LayeredWorld) {
                LayeredWorld lWorld = (LayeredWorld) world;
                lWorld.getLayer(LayerPosition.STAGE).moveActors(-1, 0);
            }
        }
    }
}
