/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author Michael
 */
public enum Edge {
    UPPER_LEFT("upperLeft"),
    UPPER_RIGHT("upperRight"),
    LOWER_LEFT("lowerLeft"),
    LOWER_RIGHT("lowerRight"),
    MIDDLE_RIGHT("middleRight"),
    MIDDLE_LEFT("middleLeft"),
    UPPER_MIDDLE("upperMiddle"),
    LOWER_MIDDLE("lowerMiddle");
    
    String edge;
    
    Edge(String edge) {
        this.edge = edge;
    }
    
    String getEdge() {
        return this.edge;
    }
}
