/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry.util;

import geometry.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michael
 * This utils class offers basic point calculations.
 */
public class PointUtils {
    /**
     * Retrieves a list of points between two given points, given a step.
     * @param point1
     * @param point2
     * @param step defines the increment between points. The chosen axis for this increment is either the y-axis when both points have the same x-value or the x-axis when not.
     * @return A list of points between the two given points.
     */
    public static List<Point> getPointsBetweenPoints(Point point1, Point point2, double step) {
        List<Point> result = new ArrayList<Point>();
        if (point1.getX() != point2.getX()) {
            double m = (double) (point1.getY() - point2.getY()) / (point1.getX() - point2.getX());
            double difX = point1.getX() - point2.getX();
            if (difX < 0) {
                difX = -difX;
            }
            if (point1.getX() < point2.getX()) {
                for (int a = 0; a <= difX; a += step) {
                    result.add(new Point(point1.getX() + a, point1.getY() + (m * a)));
                }
            } else if (point1.getX() > point2.getX()) {
                for (int a = 0; a <= difX; a += step) {
                    result.add(new Point(point2.getX() + a, point2.getY() + (m * a)));
                }
            }
        }
        else {
            for(int a = 0; a <= getDistanceBetweenPoints(point1, point2); a += step) {
                result.add(new Point(point1.getX(), point1.getY() + a));
            }
        }
        
        return result;
    }
    
    /**
     * Retrieves the distance between two points, by using Pythagoras.
     * @param point1
     * @param point2
     * @return 
     */
    public static double getDistanceBetweenPoints(Point point1, Point point2) {
        double difY = point1.getY() - point2.getY();
        double difX = point1.getX() - point2.getX();
        double difXSquared = difX * difX;
        double difYSquared = difY * difY;
        return java.lang.Math.sqrt(difXSquared + difYSquared);
    }
}
