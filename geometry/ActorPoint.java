/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

import greenfoot.Actor;

/**
 * @author Michael
 * This class is just a lay out for an eventual implementation:
 * It attaches an Actor to a certain point
 */
public class ActorPoint extends Point {
    Actor actor;
    public ActorPoint(int x, int y) {
        super(x, y);
    }

    public ActorPoint(double x, double y) {
        super(x, y);
    }
    
    public ActorPoint(int x, int y, Actor actor) {
        super(x, y);
        this.actor = actor;
    }

    public ActorPoint(double x, double y, Actor actor) {
        super(x, y);
        this.actor = actor;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }
}
