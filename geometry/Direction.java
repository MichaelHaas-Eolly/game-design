/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author Michael
 * This enum indicates either via string or enum value, which direction a given actor is facing.
 */
public enum Direction {
    LEFT("left"),
    RIGHT("right"),
    UP("up"),
    DOWN("down"),
    IDLE("idle");
    
    String direction;
    
    Direction(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }
}
