/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worlds;

import actors.characters.PopKnight;
import actors.doodads.Transition;
import worlds.layers.LayerPosition;

/**
 *
 * @author Michael
 */
public abstract class LevelWorld extends LayeredWorld {
    public abstract void populate();
    public void addTransition(LevelWorld world, LayerPosition position, int xPos, int yPos) {
        getLayer(position).addLayeredActor(new Transition(world), xPos, yPos);
    }
    
    public void addTransition(LevelWorld world, LayerPosition position, int xPos, int yPos, int xSpawn, int ySpawn) {
        getLayer(position).addLayeredActor(new Transition(world, xSpawn, ySpawn), xPos, yPos);
    }
    
    /**
     * Retrieves the pop knight character in any interconnected world
     * @return 
     */
    public PopKnight getMainCharacter() {
        return getObjects(PopKnight.class).get(0);
    }
    
}
