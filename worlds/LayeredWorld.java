/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worlds;

import worlds.layers.Layer;
import actors.obstacles.Crate;
import greenfoot.Actor;
import greenfoot.GreenfootImage;
import greenfoot.World;
import java.util.ArrayList;
import java.util.List;
import actors.LayeredActor;
import actors.characters.PopKnight;
import actors.obstacles.Block;
import java.util.HashMap;
import java.util.Map;
import worlds.layers.LayerPosition;

/**
 *
 * @author Michael
 */
public class LayeredWorld extends World {
    protected Map<LayerPosition, Layer> mappedLayers = new HashMap<>();

    public LayeredWorld() {
        super(WorldFacade.getWORLD_WIDTH(), WorldFacade.getWORLD_HEIGHT(), WorldFacade.getWORLD_CELL_SIZE(), false);
    }

    public Layer getLayer(LayerPosition position) {
        return mappedLayers.get(position);
    }

    public void addLayer(LayerPosition position, Layer layer) {
        if(layer.getLayerPosition() == null) {
            layer.setLayerPosition(position);
        }
        mappedLayers.put(position, layer);
    }


    /**
     * Overrides the repaint method of the world to iterate over all existing layers of the world.
     */
    @Override
    public void repaint() {
        destroyLayers();
        for (LayerPosition position : LayerPosition.values()) {
            Layer layer = mappedLayers.get(position);
            if (layer != null) {
                for (LayeredActor actor : layer.getLayeredActors()) {
                    addObject(actor, actor.getLayerX(), actor.getLayerY());
                    actor.setLayerX(actor.getX());
                    actor.setLayerY(actor.getY());
                }
            }
        }
    }

    public void repaintLayer(LayerPosition position) {
        destroyLayer(position);
        Layer layer = mappedLayers.get(position);
        for (LayeredActor actor : layer.getLayeredActors()) {
            addObject(actor, actor.getLayerX(), actor.getLayerY());
            actor.setLayerX(actor.getX());
            actor.setLayerY(actor.getY());
        }
    }

    public void destroyLayers() {
        for (LayerPosition position : LayerPosition.values()) {
            Layer layer = mappedLayers.get(position);
            if (layer != null) {
                for (LayeredActor actor : layer.getLayeredActors()) {
                    removeObject(actor);
                }
            }
        }

    }

    public void destroyLayer(LayerPosition position) {
        Layer layer = mappedLayers.get(position);
        for (LayeredActor actor : layer.getLayeredActors()) {
            removeObject(actor);
        }
    }
}
