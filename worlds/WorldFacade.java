/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worlds;

import actors.characters.PopKnight;
import actors.doodads.Transition;
import greenfoot.Greenfoot;
import greenfoot.World;
import java.util.HashSet;
import java.util.Set;
import worlds.layers.LayerPosition;
import worlds.levels.BaseLevel01;
import worlds.levels.BaseLevel02;

/**
 *
 * @author Michael
 * This class is meant to handle all the world settings and interchangeable worlds. It holds all information regarding these.
 */
public class WorldFacade extends World {
    private static final int WORLD_WIDTH = 800;
    private static final int WORLD_HEIGHT = 500;
    private static final int WORLD_CELL_SIZE = 1;
    
    private static PopKnight mainCharacter = new PopKnight();
    private static LevelWorld currentWorld;
    
    public WorldFacade() {
        super(0,0,0);
        init();
    }
    
    /**
     * This method will initialize every single world and will set a current world for the game.
     * It is crucial to only update this method in terms of setting up game levels.
     * As a single point of entrance, this world also sets the transitions between worlds. Use the addTransition-method inherited from the InterconnectedWorld-class to do so.
     */
    public void init() {
        PopKnight knight = new PopKnight();
        
        BaseLevel01 level1 = new BaseLevel01();
        changeWorldTo(level1);
        BaseLevel02 level2 = new BaseLevel02();
        //This adds a transition to the stage-layer that leads to level 2 at a given location.
        
    }
    
    /**
     * This method can be used to change the world manually. It is not connected to any actor.
     * @param world 
     */
    public static void changeWorldTo(LevelWorld world) {
        currentWorld = world;
        Greenfoot.setWorld(world);
    }
    
    /**
     * This method is called from a transition and ONLY from a transition.
     * @param world
     * @param transition 
     */
    public static void changeWorldTo(LevelWorld world, Transition transition) {
        currentWorld = world;
        Greenfoot.setWorld(world);
        currentWorld.getMainCharacter().setLocation((int) transition.getSpawnPoint().getX(), (int) transition.getSpawnPoint().getY());
    } 
    
    public static LevelWorld getCurrentWorld() {
        return currentWorld;
    }

    public static PopKnight getMainCharacter() {
        return mainCharacter;
    }

    public static int getWORLD_HEIGHT() {
        return WORLD_HEIGHT;
    }

    public static int getWORLD_WIDTH() {
        return WORLD_WIDTH;
    }

    public static int getWORLD_CELL_SIZE() {
        return WORLD_CELL_SIZE;
    }
}
