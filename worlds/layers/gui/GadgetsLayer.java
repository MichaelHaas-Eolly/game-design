/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worlds.layers.gui;
import worlds.LayeredWorld;
import worlds.layers.Layer;

/**
 * @author Michael
 * This layer is meant to hold all the data regarding the gadgets inside of the game
 */
public class GadgetsLayer extends Layer {
    public GadgetsLayer(LayeredWorld world) {
        super(world);
    }
}
