/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worlds.layers;

import greenfoot.Actor;
import greenfoot.World;
import java.util.ArrayList;
import java.util.List;
import actors.LayeredActor;
import actors.types.Scroller;
import greenfoot.GreenfootImage;
import worlds.LayeredWorld;

/**
 *
 * @author Michael
 */
public class Layer {
    //If fixated is true, layer can't move. This applies to GUI layers.
    protected boolean fixated = false;
    //If overlay is true, the game will be paused and the background will be blurred.
    protected boolean overlay = false;
    
    protected GreenfootImage layerImage;
    protected int width, height, cellSize;
    protected List<LayeredActor> layeredActors = new ArrayList<>();
    protected LayeredWorld world;
    
    protected LayerPosition layerPosition;
    
    
    public Layer(LayeredWorld world) {
        this.world = world;
    }
    
    public Layer(LayeredWorld world, GreenfootImage layerImage) {
        this.world = world;
        this.layerImage = layerImage;
    }
    
    public Layer(boolean fixated, boolean overlay, LayeredWorld world) {
        this.fixated = fixated;
        this.overlay = overlay;
        this.world = world;
    }

    public Layer(GreenfootImage layerImage, int width, int height, int cellSize, LayeredWorld world) {
        this.layerImage = layerImage;
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        this.world = world;
    }

    public Layer(GreenfootImage layerImage, int width, int height, int cellSize, LayeredWorld world, LayerPosition layerPosition) {
        this.layerImage = layerImage;
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        this.world = world;
        this.layerPosition = layerPosition;
    }
    
    

    
    public void addLayeredActor(LayeredActor actor) {
        layeredActors.add(actor);
        world.repaint();
    }
    
    public void moveActors(int xSpeed, int ySpeed) {
        for (LayeredActor actor : layeredActors) {
            if(!(actor instanceof Scroller)) {
                actor.setLocation(actor.getX() + xSpeed, actor.getY() + ySpeed);
            }
        }
    }

    public void addLayeredActor(LayeredActor layeredActor, int x, int y) {
        layeredActor.setLayerX(x);
        layeredActor.setLayerY(y);
        layeredActor.setLayer(this);
        layeredActors.add(layeredActor);
        world.repaint();
    }

    public LayeredWorld getWorld() {
        return world;
    }

    public void setWorld(LayeredWorld world) {
        this.world = world;
    }
    
    
    
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getCellSize() {
        return cellSize;
    }

    public void setCellSize(int cellSize) {
        this.cellSize = cellSize;
    }

    public List<LayeredActor> getLayeredActors() {
        return layeredActors;
    }

    public void setLayeredActors(List<LayeredActor> layeredActors) {
        this.layeredActors = layeredActors;
    }

    public boolean isFixated() {
        return fixated;
    }

    public void setFixated(boolean fixated) {
        this.fixated = fixated;
    }

    public boolean isOverlay() {
        return overlay;
    }

    public void setOverlay(boolean overlay) {
        this.overlay = overlay;
    }

    public GreenfootImage getLayerImage() {
        return layerImage;
    }

    public void setLayerImage(GreenfootImage layerImage) {
        this.layerImage = layerImage;
    }

    public LayerPosition getLayerPosition() {
        return layerPosition;
    }

    /**
     * If the layer position switches for the layer, the LayerPosition of all LayeredActors is updated.
     * @param layerPosition 
     */
    public void setLayerPosition(LayerPosition layerPosition) {
        for(LayeredActor la: layeredActors) {
            la.setLayerPosition(layerPosition);
        }
        this.layerPosition = layerPosition;
    }
    
    
}
