/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worlds.layers;

/**
 *
 * @author Michael
 */
public enum LayerPosition {
    //World positions
    BACKGROUND,
    STAGE,
    FOREGROUND,
    //GUI positions
    OPTIONS,
    GADGET_OVERVIEW,
    ITEM_OVERVIEW;        
}
