package worlds.levels;

import actors.LayeredActor;
import actors.characters.PopKnight;
import actors.doodads.Transition;
import actors.obstacles.Block;
import actors.obstacles.Crate;
import worlds.LevelWorld;
import worlds.WorldFacade;
import worlds.layers.Layer;
import worlds.layers.LayerPosition;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Michael
 */
public class BaseLevel02 extends LevelWorld {
    public BaseLevel02() {
        populate();
    }

    @Override
    public void populate() {
        Layer stage = new Layer(this);
        Block ground = new Block();
        for (int a = 0; a < 20; a++) {
            stage.addLayeredActor(new Block(), a * ground.getImage().getWidth(), getHeight() - (ground.getImage().getHeight() / 2));
        }
        stage.addLayeredActor(new PopKnight(), 200, 200);
        stage.addLayeredActor(new Crate(), 200, 200);
        
        addLayer(LayerPosition.STAGE, stage);
        repaint();
    }
}
