/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worlds.levels;

import controls.ControlActor;
import actors.LayeredActor;
import actors.characters.PopKnight;
import actors.doodads.Transition;
import actors.obstacles.Block;
import actors.obstacles.Crate;
import worlds.LevelWorld;
import worlds.WorldFacade;
import worlds.layers.Layer;
import worlds.layers.LayerPosition;

/**
 *
 * @author Michael
 */
public class BaseLevel01 extends LevelWorld {
    
    public BaseLevel01() {
        populate();
    }

    @Override
    public void populate() {
        Layer stage = new Layer(this);
        Block ground = new Block();
        for (int a = 0; a < 20; a++) {
            stage.addLayeredActor(new Block(), a * ground.getImage().getWidth(), getHeight() - (ground.getImage().getHeight() / 2));
        }
        
        stage.addLayeredActor(new PopKnight(), 100, 200);
        stage.addLayeredActor(new Crate(), 200, 200);
        stage.addLayeredActor(new ControlActor(), 200, 200);
        for(LayeredActor la: stage.getLayeredActors()) {
            System.out.println(la.getClass());
        }
        addLayer(LayerPosition.STAGE, stage);
        repaint();
    }
    
    
}
