/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors;

import geometry.Edge;
import geometry.Point;
import greenfoot.Actor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Michael
 * This class extends the basic Greenfoot actor to add additional geometry functions.
 */
public abstract class PointExtendedActor extends LayeredActor {
    Set<Point> polygonPoints = new HashSet<>();
    
    
    public void addPointToPolygon(Point point) {
        //TODO: Polygon collision!!!
    }
    /**
     * @param edge
     * @return Point at the edge of an image. 
     */
    public Point getPointAtImageEdge(Edge edge) {
        switch (edge) {
            case UPPER_LEFT: {
                return new Point(-getImage().getHeight() / 2, -getImage().getHeight() / 2);
            }
            case UPPER_RIGHT: {
                return new Point(getImage().getWidth() / 2, -getImage().getHeight() / 2);
            }
            case LOWER_LEFT: {
                return new Point(-getImage().getWidth() / 2, getImage().getHeight() / 2 + 5);
            }
            case LOWER_RIGHT: {
                return new Point(getImage().getWidth() / 2, getImage().getHeight() / 2 + 5);
            }
            case MIDDLE_RIGHT: {
                return new Point(getImage().getWidth() / 2, 0);
            }
            case MIDDLE_LEFT: {
                return new Point(-getImage().getWidth() / 2, 0);
            }
            case UPPER_MIDDLE: {
                return new Point(0, -getImage().getHeight() / 2);
            }
            case LOWER_MIDDLE: {
                return new Point(0, getImage().getHeight() / 2 + 5);
            }
            default:
                return null;
        }
    }
    /**
     * Returns a map of all Points of at an image's edges.
     * @return Map<Edge, Point>, the key-value edge references a given point at a given edge
     */
    public Map<Edge, Point> getAllPointsAtImageEdge() {
        Map<Edge, Point> result = new HashMap<>();
        result.put(Edge.UPPER_LEFT, getPointAtImageEdge(Edge.UPPER_LEFT));
        result.put(Edge.UPPER_RIGHT, getPointAtImageEdge(Edge.UPPER_RIGHT));
        result.put(Edge.LOWER_LEFT, getPointAtImageEdge(Edge.LOWER_LEFT));
        result.put(Edge.LOWER_RIGHT, getPointAtImageEdge(Edge.LOWER_RIGHT));
        result.put(Edge.LOWER_MIDDLE, getPointAtImageEdge(Edge.LOWER_MIDDLE));
        result.put(Edge.UPPER_MIDDLE, getPointAtImageEdge(Edge.UPPER_MIDDLE));
        result.put(Edge.MIDDLE_RIGHT, getPointAtImageEdge(Edge.MIDDLE_RIGHT));
        result.put(Edge.MIDDLE_LEFT, getPointAtImageEdge(Edge.MIDDLE_LEFT));
        return result;
    }

    public Set<Point> getPoints() {
        return polygonPoints;
    }

    public void setPoints(Set<Point> points) {
        this.polygonPoints = points;
    }
}
