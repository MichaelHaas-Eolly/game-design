/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors;

import geometry.Direction;
import actors.obstacles.Crate;
import actors.obstacles.Interactable;
import greenfoot.Actor;
import greenfoot.GreenfootImage;
import actors.obstacles.Block;
import actors.obstacles.Collider;

/**
 * @author Michael
 * Basic extended actor class.
 * Includes:
 * - Gravity options
 * - Animation life cycle options
 * Uses getImagePointer() to build an actual string out of a given number within an animation cycle.
 * Path values except IMAGE_FOLDER should be overwritten by the subclassing class!
 */
public abstract class AnimationActor extends MovementActor {
    /**
     * Animation life cycle options
     */
    //The following path is mandatory for all animated actors.
    protected final static String IMAGE_FOLDER = "\\images\\animated";
    
    
    
    //Points at a given image within an animation cycle. Resets via resetAnimation()-method.
    protected int imagePointer = 0;
    protected int imageMaximum = 100;
    
    
    
    
    
    public void interact() {
        Interactable interactable = (Interactable) getOneIntersectingObject(Interactable.class);
        if(interactable != null) {
           interactable.interact(this);
        }
    }
    
    /**
     * Resets all animations in progress to the idle file.
     */
    protected abstract void resetAnimation();
    protected abstract void animateWalk();
    protected abstract void animateStandingStill();
    /*
     * Returns a string reference from numbers between 0-999 with three written out digits, thus resulting in a total number of 1000 images, if needed.
     * @return 
     */
    protected String getImagePointer() {
        if(imagePointer < 10) {
            return "00" + imagePointer;
        }
        else if(10 <= imagePointer &&
                imagePointer < 100) {
            return "0" + imagePointer;
            
        }
        else {
            return "" + imagePointer;
        }
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    
}
