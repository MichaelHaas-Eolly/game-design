/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors.types;

/**
 *
 * @author Michael
 * Classes which implement the scroller interface will not be scrolled, but instead scroll the world.
 */
public interface Scroller {
    public void scrollWorld();
    public void recenterCamera();
}
