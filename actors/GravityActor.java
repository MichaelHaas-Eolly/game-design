/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors;

import actors.states.MovementState;
import geometry.Direction;
import geometry.Edge;
import greenfoot.Actor;
import actors.obstacles.Collider;

/**
 *
 * @author Michael
 */
public abstract class GravityActor extends CollisionActor {
    private static int VERTICAL_SPEED = 0;
    private static final int ACCELERATION = 1;
    
    @Override
    public void act() {
        checkFall();
    }
    
    protected boolean onGround() {
        Actor ground = (Actor) getObstacleToThe(Direction.DOWN, true);
        if(ground == null) {
            return false;
        }
        else {
            if(isInLayer((LayeredActor) ground)) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    
    public void moveToGround(Actor ground)
    {
        int groundHeight = ground.getImage().getHeight();
        int newY = ground.getY() - (groundHeight + getImage().getHeight())/2;
        setLocation(getX(), newY);
    }
    public void fall()
    {
        setLocation(getX(), getY() + VERTICAL_SPEED);
        if(VERTICAL_SPEED <=9)
        {
            VERTICAL_SPEED = VERTICAL_SPEED + ACCELERATION;
        }
    }
    
    protected void checkFall()
    {
        if(onGround())
        {
            VERTICAL_SPEED = 0;
        }
        else
        {
            fall();
        }
    }
}
