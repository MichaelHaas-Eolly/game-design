/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors.doodads;

import actors.AnimationActor;
import actors.CollisionActor;
import actors.obstacles.Interactable;
import geometry.Point;
import greenfoot.GreenfootImage;
import worlds.WorldFacade;
import actors.obstacles.Collider;
import geometry.Direction;
import worlds.LevelWorld;

/**
 *
 * @author Michael
 */
public class Transition extends CollisionActor implements Interactable, Collider {
    /**
     * Spawn point refers to the point where the pop corn character is spawned in the next world.
     */
    Point spawnPoint;
    LevelWorld levelWorld;
    
    public Transition(LevelWorld interconnectedWorld) {
        this.levelWorld = interconnectedWorld;
        setImage(new GreenfootImage("\\images\\void\\BlackImage 20x100.png"));
    }
    
    public Transition(LevelWorld interconnectedWorld, int xSpawn, int ySpawn) {
        this.levelWorld = interconnectedWorld;
        this.spawnPoint = new Point(xSpawn, ySpawn);
        setImage(new GreenfootImage("\\images\\void\\BlackImage 20x100.png"));
    }
    
    public void setWorldTransition(LevelWorld world) {
        levelWorld = world;
    }
    
    public void setSpawnPointOffset(int xSpawn, int ySpawn) {
        spawnPoint = new Point(xSpawn, ySpawn);
    }
    
    @Override
    public void interact(AnimationActor actor) {
        if(WorldFacade.getCurrentWorld() != levelWorld) {
            WorldFacade.changeWorldTo(levelWorld, this);
        }
    }

    public Point getSpawnPoint() {
        return spawnPoint;
    }

    public void setSpawnPoint(Point spawnPoint) {
        this.spawnPoint = spawnPoint;
    }

    public LevelWorld getLevelWorld() {
        return levelWorld;
    }

    public void setLevelWorld(LevelWorld levelWorld) {
        this.levelWorld = levelWorld;
    }

    
    
    
}
