/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors.characters;

import actors.AnimationActor;
import actors.animation.AnimationLifeCycle;
import actors.obstacles.Collider;
import geometry.Direction;
import actors.obstacles.Interactable;
import actors.states.MovementState;
import actors.types.Scroller;
import exceptions.AnimationLifeCycleException;
import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.GreenfootImage;
import greenfoot.World;
import worlds.LayeredWorld;
import worlds.layers.LayerPosition;

/**
 *
 * @author Michael
 */
public class PopKnight extends AnimationActor implements Collider, Scroller {
    /**
     * Animation life cycle options
     */
    //The following describes a specific actor name and should be overwritten on construction of the subclass. This is just an example.
    protected String imageFolderActorPath = IMAGE_FOLDER + "\\KnightPop";
    //Files that ARE PART OF AN ANIMATION CYCLE should be placed within a given subfolder, named accordingly. Overwrite this value when subclassing.
    //Examples: walkingRight_001.png, walkingRight_010.png, walkingRight_100.png
    //The full image path will be: images\animated\actor001\walkingRight\img_$.png
    protected String imageFolderWalkingRightPath = imageFolderActorPath + "\\walkingRight\\walkingRight_$.png";
    protected String imageFolderWalkingLeftPath = imageFolderActorPath + "\\walkingLeft\\walkingLeft_$.png";
    protected String imageFolderStandingStillRight = imageFolderActorPath + "\\standingStill\\standingStill_$.png";
    //Basic files that ARE NOT PART OF AN ANIMATION CYCLE should be placed directly inside the specific actor_name path, given above.
    //The full image path will be: images\animated\actor001\reset.png
    protected String imageResetReferencePath = imageFolderActorPath + "\\reset.png";
   
    
    public PopKnight() {
        setImage(new GreenfootImage(imageResetReferencePath));
        imageMaximum = 50;
    }
    

    @Override
    public void act() {
        direction = Direction.IDLE;
        checkKey();
        move();
        checkRightObstacles();
        checkLeftObstacles();
        checkFall();
        animateWalk();
    }
    
    @Override
    public void move() {
        World world = getWorld();
        if (world instanceof LayeredWorld) {
            scrollWorld();
        } else {
            switch (direction) {
                case LEFT: {
                    setLocation(getX() - walkingSpeed, getY());
                    break;
                }
                case RIGHT: {
                    setLocation(getX() + walkingSpeed, getY());
                    break;
                }
            }
        }
        animateWalk();
    }
    
    @Override
    public void checkKey() {
        if(Greenfoot.isKeyDown("right"))
        {
            direction = Direction.RIGHT;
        }
        if(Greenfoot.isKeyDown("left"))
        {
            direction = Direction.LEFT;
        }
        if(Greenfoot.isKeyDown("up") && movementState != MovementState.JUMPING)
        {
            jump();
        }
    }

    @Override
    public void scrollWorld() {
        World world = getWorld();
        if (world instanceof LayeredWorld) {
            LayeredWorld lWorld = (LayeredWorld) world;
            switch (direction) {
                case LEFT: {
                    getLayer().moveActors(walkingSpeed, 0);
                    break;
                }
                case RIGHT: {
                    getLayer().moveActors(-walkingSpeed, 0);
                    break;
                }
            }
        }
    }

    @Override
    public void recenterCamera() {
         getLayer().moveActors(getX(), 0);
    }

    @Override
    public void stopByRightObstacle(Actor rightObstacle)
    {
        int obstacleWidth = rightObstacle.getImage().getWidth();
        int newX = rightObstacle.getX() - (obstacleWidth + getImage().getWidth())/2;
        if(rightObstacle instanceof Interactable) {
            Interactable interactable = (Interactable) rightObstacle;
            interactable.interact(this);
        }
        else setLocation(newX - 5, getY());
    }
    
    @Override
    public void stopByLeftObstacle(Actor leftObstacle) {
        int obstacleWidth = leftObstacle.getImage().getWidth();
        int newX = leftObstacle.getX() + (obstacleWidth + getImage().getWidth())/2;
        if(leftObstacle instanceof Interactable) {
            Interactable interactable = (Interactable) leftObstacle;
            interactable.interact(this);
        }
        else setLocation(newX + 5, getY());
    }
    
    
    
    @Override
    protected void animateWalk() {
        switch(direction) {
            case RIGHT: {
                setImage(new GreenfootImage(imageFolderWalkingRightPath.replace("$", getImagePointer())));
                break;
            }
            case LEFT: {
                setImage(new GreenfootImage(imageFolderWalkingRightPath.replace("$", getImagePointer())));
                break;
            }
            case IDLE: {
                resetAnimation();
            }
        }
        if(imagePointer < imageMaximum - 1) {
            imagePointer++;
        }
        else {
            imagePointer = 0;
        }
    }

    @Override
    protected void animateStandingStill() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    @Override
    protected void resetAnimation() {
        imagePointer = 0;
        setImage(new GreenfootImage(imageResetReferencePath));
    }
}
