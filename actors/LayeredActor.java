/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors;

import greenfoot.Actor;
import worlds.layers.Layer;
import worlds.layers.LayerPosition;

/**
 *
 * @author Michael
 */
public class LayeredActor extends Actor {
    protected Layer layer;
    protected int layerX, layerY;
    protected LayerPosition layerPosition;

    /**
     * Checks if a given actor is in the layer of this actor.
     * @param actor actor to be checked
     * @return TRUE: When there is no layer given (meaning that the actor will collide with everything) or when the actor is in the layer; 
     * FALSE: if the actor is not in the layer
     */
    public boolean isInLayer(LayeredActor actor) {
        if(layer == null) {
            return true;
        }
        else {
            return layer.getLayeredActors().contains(actor);
        }
    }
    
    public int getLayerX() {
        return layerX;
    }
    
    public void moveHorizontally(int speed) {
        setLayerX(layerX + speed);
    }
    
    public void moveVertically(int speed) {
        setLayerY(layerY + speed);
    }

    public void setLayerX(int layerX) {
        this.layerX = layerX;
    }

    public int getLayerY() {
        return layerY;
    }

    public void setLayerY(int layerY) {
        this.layerY = layerY;
    }

    public Layer getLayer() {
        return layer;
    }

    public void setLayer(Layer layer) {
        this.layer = layer;
    }

    public LayerPosition getLayerPosition() {
        return layerPosition;
    }

    public void setLayerPosition(LayerPosition layerPosition) {
        this.layerPosition = layerPosition;
    }
    
    
}
