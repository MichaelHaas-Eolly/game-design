/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors.animation;

import actors.AnimationActor;
import actors.states.MovementState;
import exceptions.AnimationLifeCycleException;
import geometry.Direction;
import java.io.File;

/**
 *
 * @author Michael
 */
public class AnimationLifeCycle {
    private AnimationActor animationActor;
    private Direction direction;
    private MovementState movementState;
    
    private File sourceFile;
    
    private final int imageMax;
    private int imagePointer = 0;
    
    
    public AnimationLifeCycle(AnimationActor animationActor, Direction direction, MovementState movementState, String pathToSourceFile) throws AnimationLifeCycleException {
        this.animationActor = animationActor;
        this.direction = direction;
        this.movementState = movementState;
        this.sourceFile = new File(pathToSourceFile);
        if(sourceFile.exists()) {
            this.imageMax = sourceFile.listFiles().length;
        }
        else {
            throw new AnimationLifeCycleException("Init failed, because source file for the given path is non-existent. Please check the input path");
        }
        System.out.println(getImagePrefix());
        System.out.println(getImagePointer());
    }
    
    public void animate() {
        imagePointer++;
        animationActor.setImage(getImagePrefix() + "_" + getImagePointer() + ".png");
    }
    
    public void resetAnimation() {
        imagePointer = 0;
    }
    
    private String getImagePrefix() {
        return (movementState.toString() + direction.toString()).toLowerCase();
    }
    
    private String getImagePointer() {
        if(imagePointer < 10) {
            return "00" + imagePointer;
        }
        else if(10 <= imagePointer &&
                imagePointer < 100) {
            return "0" + imagePointer;
            
        }
        else {
            return "" + imagePointer;
        }
    }
    
}
