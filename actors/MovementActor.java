/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors;

import actors.obstacles.Block;
import greenfoot.Actor;
import actors.states.MovementState;
import geometry.Direction;
import geometry.Edge;
import actors.obstacles.Collider;

/**
 *
 * @author Michael
 * Adds gravity and movement to the extending class.
 * Extending classes need to implement the checkFall() method in the act method!!!
 */
public abstract class MovementActor extends GravityActor {
    //Defines the facing direction of the actor
    protected Direction direction = Direction.RIGHT;
    protected int baseWalkingSpeed = 2;
    protected int walkingSpeed = baseWalkingSpeed;
    protected MovementState movementState = MovementState.WALKING;
    protected int jumpingStrength = 16;
    protected int vSpeed = 1;
    protected int acceleration = 1;
    
    public abstract void checkKey();
    
    public void move() {
        switch(direction) {
            case LEFT: {
                setLocation(getX() - walkingSpeed, getY());
                break;
            }
            case RIGHT: {
                setLocation(getX() + walkingSpeed, getY());
                break;
            }
        }
    }
    
    public void jump()
    {
        vSpeed = vSpeed - jumpingStrength;
        movementState = MovementState.JUMPING;
        fall();
    }
    
    @Override
    protected boolean onGround() {
        LayeredActor ground = (LayeredActor) getObstacleToThe(Direction.DOWN, true);
        if(ground == null) {
            movementState = MovementState.JUMPING;
            return false;
        }
        else {
            if(isInLayer(ground)) {
                moveToGround(ground);
                return true;
            }
            else return false;
        }
    }
   
    public boolean platformAbove()
    {
        LayeredActor ceiling = (LayeredActor) getOneObjectAtOffset(0, (int) getPointAtImageEdge(Edge.UPPER_MIDDLE).getY(), Collider.class);
        if(ceiling != null)
        {
            vSpeed = 1;
            if(isInLayer(ceiling)) {
                bopHead(ceiling);
                return true;
            }
            else return false;
        }
        else
        {
            return false;
        }
    }
    
    public void bopHead(Actor ceiling)
    {
        int ceilingHeight = ceiling.getImage().getHeight();
        int newY = ceiling.getY() + (ceilingHeight + getImage().getHeight())/2;
        setLocation(getX(), newY);
    }
    
    @Override
    public void moveToGround(Actor ground)
    {
        int groundHeight = ground.getImage().getHeight();
        int newY = ground.getY() - (groundHeight + getImage().getHeight())/2;
        setLocation(getX(), newY);
        movementState = MovementState.WALKING;
    }
    
    @Override
    public void fall()
    {
        setLocation(getX(), getY() + vSpeed);
        if(vSpeed <=9)
        {
            vSpeed = vSpeed + acceleration;
        }
        movementState = MovementState.JUMPING;
    }
    
    @Override
    protected void checkFall()
    {
        if(onGround())
        {
            vSpeed = 0;
        }
        else
        {
            fall();
        }
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public int getWalkingSpeed() {
        return walkingSpeed;
    }

    public void setWalkingSpeed(int walkingSpeed) {
        this.walkingSpeed = walkingSpeed;
    }

    public MovementState getMovementState() {
        return movementState;
    }

    public void setMovementState(MovementState movementState) {
        this.movementState = movementState;
    }

    public int getJumpingStrength() {
        return jumpingStrength;
    }

    public void setJumpingStrength(int jumpingStrength) {
        this.jumpingStrength = jumpingStrength;
    }

    public int getvSpeed() {
        return vSpeed;
    }

    public void setvSpeed(int vSpeed) {
        this.vSpeed = vSpeed;
    }

    public int getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(int acceleration) {
        this.acceleration = acceleration;
    }
    
    
}
