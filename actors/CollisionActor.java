/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors;

import actors.obstacles.Interactable;
import geometry.Direction;
import greenfoot.Actor;
import geometry.Edge;
import geometry.Point;
import geometry.util.PointUtils;
import java.util.List;
import java.util.Map;
import actors.obstacles.Collider;

/**
 * @author Michael
 * Adds collision detection to the left and right side
 */
public abstract class CollisionActor extends PointExtendedActor {
    protected static final double  COLLISION_STEP = 10;
    
    public boolean checkRightObstacles() {
        LayeredActor rightObstacle = (LayeredActor) getObstacleToThe(Direction.RIGHT, false);
       
        if(rightObstacle == null) {
            return false;
        }
        else {
            if(isInLayer(rightObstacle)) {
                stopByRightObstacle(rightObstacle);
                return true;
            }
            else {
                return false;
            }
        }
    }
    
    public void stopByRightObstacle(Actor rightObstacle)
    {
        int obstacleWidth = rightObstacle.getImage().getWidth();
        int newX = rightObstacle.getX() - (obstacleWidth + getImage().getWidth())/2;
        setLocation(newX - 5, getY());
    }
    
    public boolean checkLeftObstacles() {
        LayeredActor leftObstacle = (LayeredActor) getObstacleToThe(Direction.LEFT, false);

        if (leftObstacle == null) {
            return false;
        } else {
            if (isInLayer(leftObstacle)) {
                stopByLeftObstacle(leftObstacle);
                return true;
            } else {
                return false;
            }
        }
    }
    
    
    
    public void stopByLeftObstacle(Actor leftObstacle)
    {
        int obstacleWidth = leftObstacle.getImage().getWidth();
        int newX = leftObstacle.getX() + (obstacleWidth + getImage().getWidth())/2;
        setLocation(newX + 5, getY());
    }
    
    public boolean checkObstacleToThe(Direction direction, boolean hardCollision) {
        return getObstacleToThe(direction, hardCollision) != null;
    }
    
    public Object getObstacleToThe(Direction direction, boolean hardCollision) {
        Object collider = null;
        if(hardCollision) {
            Point point1 = getPointAtImageEdge(Edge.MIDDLE_LEFT);
            Point point2 = getPointAtImageEdge(Edge.MIDDLE_RIGHT);
            switch (direction) {
                case LEFT: {
                    point1 = getPointAtImageEdge(Edge.UPPER_LEFT);
                    point2 = getPointAtImageEdge(Edge.LOWER_LEFT);
                    break;
                }
                case RIGHT: {
                    point1 = getPointAtImageEdge(Edge.UPPER_RIGHT);
                    point2 = getPointAtImageEdge(Edge.LOWER_RIGHT);
                    break;
                }
                case UP: {
                    point1 = getPointAtImageEdge(Edge.UPPER_RIGHT);
                    point2 = getPointAtImageEdge(Edge.UPPER_LEFT);
                    break;
                }
                case DOWN: {
                    point1 = getPointAtImageEdge(Edge.LOWER_LEFT);
                    point2 = getPointAtImageEdge(Edge.LOWER_RIGHT);
                    break;
                }
            }
            for (Point p : PointUtils.getPointsBetweenPoints(point1, point2, 1)) {
                Object obstacle = getObstacleAtPoint(p);
                if (obstacle != null) {
                    return getObstacleAtPoint(p);
                }
            }
        }
        else {
            switch (direction) {
                case LEFT: {
                    collider = getObstacleAtPoint(getPointAtImageEdge(Edge.MIDDLE_LEFT));
                    break;
                }
                case RIGHT: {
                    collider = getObstacleAtPoint(getPointAtImageEdge(Edge.MIDDLE_RIGHT));
                    break;
                }
                case UP: {
                    collider = getObstacleAtPoint(getPointAtImageEdge(Edge.UPPER_MIDDLE));
                    break;
                }
                case DOWN: {
                    collider = getObstacleAtPoint(getPointAtImageEdge(Edge.LOWER_MIDDLE));
                    break;
                }
            }
        }
        
        
        
        return collider;
    }
    
    protected Object getObstacleAtPoint(Point point) {
        return getOneObjectAtOffset((int) point.getX(), (int) point.getY(), Collider.class);
    }
}
