/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors.obstacles;

import actors.AnimationActor;
import greenfoot.Actor;

/**
 *
 * @author Michael
 */
public interface Interactable {
    public void interact(AnimationActor actor);
}
