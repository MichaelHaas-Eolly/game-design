/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors.obstacles;

import actors.AnimationActor;
import actors.GravityActor;
import actors.MovementActor;
import geometry.Direction;
import greenfoot.Actor;
import greenfoot.GreenfootImage;

/**
 *
 * @author Michael
 */
public class Crate extends GravityActor implements Interactable, Destructable, Collider {
    
    public Crate() {
        setImage(new GreenfootImage("\\images\\obstacles\\Crate 46x52.png"));
    }
    

    @Override
    public void interact(AnimationActor interactingActor) {
        switch(interactingActor.getDirection()) {
            case RIGHT: {
                //Check if interactingActor is on the left side of the crate
                Object collider = getObstacleToThe(Direction.LEFT, false);
                if(collider != null &&
                        collider.equals(interactingActor)) {
                    setLocation(getX() + interactingActor.getWalkingSpeed(), getY());
                }
                //TODO: Pulling
                break;
            }
            case LEFT: {
                //Check if interactingActor is on the right side of the crate
                Object collider = getObstacleToThe(Direction.RIGHT, false);
                if(collider != null &&
                        collider.equals(interactingActor)) {
                    setLocation(getX() - interactingActor.getWalkingSpeed(), getY());
                }
                //TODO: Pulling
                break;
            }
        }
    }
   
    @Override
    public void destroy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
