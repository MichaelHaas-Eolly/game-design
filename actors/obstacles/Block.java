/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actors.obstacles;

import actors.CollisionActor;
import greenfoot.Actor;
import greenfoot.GreenfootImage;

/**
 *
 * @author Michael
 */
public class Block extends CollisionActor implements Collider {
    public Block() {
        setImage(new GreenfootImage("\\images\\obstacles\\Block 46x52.png"));
        
    }
}
