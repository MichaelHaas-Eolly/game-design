/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

import actors.animation.AnimationLifeCycle;

/**
 *
 * @author Michael
 */
public class AnimationLifeCycleException extends Exception {
    public AnimationLifeCycleException(AnimationLifeCycle animationLifeCycle) {
        super("AnimationLifeCycleException: " + animationLifeCycle);
    }
    
    public AnimationLifeCycleException(String initFailedMessage) {
        super(initFailedMessage);
    }
}
